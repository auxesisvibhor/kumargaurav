// Owl carousel

$('.owl1').owlCarousel({
    loop: true,
	autoplay:true,	   
    margin: 10,
    items: 1,
    dots: true,
    dotsEach: true,
    animateOut: 'slideOutUp',
    animateIn: 'slideInUp',
    autoplayHoverPause: true,
	mouseDrag:false,
	touchDrag:false
});
$('.owl2').owlCarousel({
    loop: true,
	autoplay:true,
	autoplayTimeout:3000,
	autoplayHoverPause: true,
    margin: 10,
	items: 4,
    nav: false,
	dots: true,
    dotsEach: false,
    responsive: {
        0: {
            items: 1
        },
        767:{
            items:2
        },
        900: {
            items: 2
        },
        1000: {
            items: 3
        },
        1200:{
            items:4
        }
    }
});
$('.video-slider').owlCarousel({
    loop: true,
	autoplay:true,
	autoplayTimeout:2000,
	autoplayHoverPause: true,
    margin: 10,
    nav: false,
	dots: true,
    dotsEach: false,
    responsive: {
        0: {
            items: 1
        },
        767:{
            items:2
        },
        900: {
            items: 2
        },
        1000: {
            items: 3
        },
        1200:{
            items:4
        }
    }
});

/*=====smooth scroll======*/
$(document).ready(function () {
    // smoothScroll
    // Select all links with hashes
	'use strict';
	var scroll = new SmoothScroll('a.scroll_down[href*="#"]');
	//$('a.scroll_down[href*="#"]');
    $('a.nav-link[href*="#"]')
    // Remove links that don't actually link to anything
    .not('[href="#"]')
    .not('[href="#0"]')
    .click(function (event) {
        // On-page links
        if (
            location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') &&
            location.hostname === this.hostname
        ) {
            // Figure out element to scroll to
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            // Does a scroll target exist?
            if (target.length) {
                // Only prevent default if animation is actually gonna happen
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000, function () {
                    // Callback after animation
                    // Must change focus!
                    var $target = $(target);
                    $target.focus();
                    if ($target.is(":focus")) { // Checking if the target was focused
                        return false;
                    } else {
                        $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                        $target.focus(); // Set focus again
                    }
                });
            }
        }
    });
});

/*======== Intro typer ===========*/
var element = $(".element");
$(function() {
	'use strict';
	element.typed({
		strings: ["Serial Entrepreneur.", "Blockchain Speaker.", "Blockchain Influencer."],
		typeSpeed: 100,
		loop: true,
	});
});


/*======== youtube popup ===========*/
$(function(){
	'use strict';
	//jQuery("a.bla-1").YouTubePopUp();
	$("a.bla-2").YouTubePopUp( { autoplay: 0 } ); 
});

/*======== Scroll Animation ===========*/

$(function() {
	'use strict';
  $(document).scroll(function(){
  	var scrollPos = $(this).scrollTop();
	//var scrollPos1 = $('.recognitions').scrollTop();
  	$('.featured_bg').css({
  	  'top' : (scrollPos/2)+'px',
  	  'opacity' : 1-(scrollPos/510)
  	});
	$('.recognitions_box').css({		
  	  'top' : (scrollPos/2)+'px',	  
  	  'opacity' : 1-(scrollPos/2580)
  	});	
	
  	$('.banner_section').css({
  	  'background-position' : 'center ' + (-scrollPos/2)+'px'
  	});
  });   
  
 
});

